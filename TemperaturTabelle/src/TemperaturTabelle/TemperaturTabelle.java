package TemperaturTabelle;

public class TemperaturTabelle {

	public static void main(String[] args) {
		int [] farenheitArray = {-20, -10, 0, 20, 30};
		System.out.println("Fareheit  |  Celsius");
		for(int farenheit : farenheitArray) {
			int i = 0;
			System.out.println(convertFahrenheit(farenheit) + " | " + toCelsius(farenheit));
			i ++;
		}
	}
	
	public static String toCelsius(int farenheit) {
		return round((farenheit - 32) * (5.0/9.0));
	}
	
	public static String round(double d)
	{
	    return String.format("%.2f", d);
	}
	
	public static String convertFahrenheit(int i) {
		String returnValue = Integer.toString(i);
		while(returnValue.length() < 3) {
			returnValue =  returnValue + " " ;
		}
		return returnValue + "      ";
	}

}
