import java.text.DecimalFormat;
import java.util.Scanner;

public class Automat {

	private Scanner tastatur = new Scanner(System.in);
	private DecimalFormat df = new DecimalFormat("0.00");

	public void fahrkartenbestellungErfassen() {
		int anzahl = 1;
		int auswahl;
		double zuZahlenderBetrag = 0.0;
		System.out.println("W�hlen Sie die gew�nschte Fahrkarte aus: ");
		System.out.println("Einzefahrausweis (1)");
		System.out.println("Tageskarte (2)");
		System.out.println("Gruppenkarte (3) ");
		auswahl = tastatur.nextInt();
		
		if(auswahl <= 3 && auswahl > 0 ) {
			if(auswahl == 1) {
				zuZahlenderBetrag = 2.9;
			}
			if(auswahl == 2) {
				zuZahlenderBetrag = 8.6;
			}
			if(auswahl == 3) {
				zuZahlenderBetrag = 23.5;
			}
		}else {
			System.out.println("Falsche eingabe, bitte erneut eingeben");
			fahrkartenbestellungErfassen();
		}

		System.out.print("Anzahl der Fahrkarten: ");
		anzahl = tastatur.nextInt();
		zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
		fahrkartenBezahlen(zuZahlenderBetrag);
	}

	public void fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: " + df.format(zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		fahrkartenAusgeben();
		rueckgeldAusgeben( eingezahlterGesamtbetrag, zuZahlenderBetrag);
	}

	public void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(200);
		}
		System.out.println("\n\n");
	}

	public void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		int r�ckgabebetrag = (int) eingezahlterGesamtbetrag * 100;
		r�ckgabebetrag = r�ckgabebetrag - ((int) (zuZahlenderBetrag * 100));
		if (r�ckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + df.format((double) r�ckgabebetrag / 100) + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 200.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "Euro");
				r�ckgabebetrag -= 200.0;
			}
			while (r�ckgabebetrag >= 100.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "Euro");
				r�ckgabebetrag -= 100.0;
			}
			while (r�ckgabebetrag >= 50.0) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag -= 50.0;
			}
			while (r�ckgabebetrag >= 20.0) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				r�ckgabebetrag -= 20.0;
			}
			while (r�ckgabebetrag >= 10.0) // 10 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag -= 10.0;
			}
			while (r�ckgabebetrag >= 5.0)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				r�ckgabebetrag -= 5.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		fahrkartenbestellungErfassen();
	}
}
